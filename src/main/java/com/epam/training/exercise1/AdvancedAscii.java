package com.epam.training.exercise1;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * advanced ascii main class.
 */
public final class AdvancedAscii {

    private static final int HEIGHT_STEPS = 60;
    private static final int WIDTH_STEPS = 200;

    private static int[][] intensities = new int[HEIGHT_STEPS][WIDTH_STEPS];

    private static int stepX;
    private static int stepY;


    private AdvancedAscii() {
    }

    /**
     * prints image using symbols to console.
     *
     * @param args for jvm
     * @throws IOException
     */
    public static void main(String[] args) {
        Image image = Image.createImage("pair_hiking.png");
        stepY = image.getHeight() / HEIGHT_STEPS;
        stepX = image.getWidth() / WIDTH_STEPS;

        IntStream.range(0, HEIGHT_STEPS).forEach(i ->
                IntStream.range(0, WIDTH_STEPS).forEach(j ->
                        countMinMaxIntensity(image, i, j)
                )
        );
        printImage(image);
    }

    private static void countMinMaxIntensity(Image image, int y, int x) {
        int windowX = x * stepX;
        int windowY = y * stepY;
        int windowIntensitySum = IntStream.range(windowY, windowY + stepY - 1).map(i ->
                IntStream.range(windowX, windowX + stepX - 1).map(j ->
                        image.getIntensity(new Coordinate(j, i)))
                        .sum()
        ).sum();
        int averageIntensity = windowIntensitySum / (stepY * stepX);

        image.setMax(averageIntensity);
        image.setMin(averageIntensity);

        intensities[y][x] = averageIntensity;
    }

    private static void printImage(Image image) {
        Arrays.stream(intensities).forEach((row) -> {
                Arrays.stream(row).forEach((el) -> System.out.print(image.getCharByIntensity(el)));
                System.out.println();
            }
        );
    }
}
