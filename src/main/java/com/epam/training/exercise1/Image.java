package com.epam.training.exercise1;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * class to represent image.
 */
public final class Image {

    private static final int LAST_BYTE = 0xFF;
    private static final int BYTE = 8;
    private static final int TWO_BYTES = 16;
    private static final int RGB_COLORS_NUMBER = 3;
    private static final int RGB_COLORS_INTENSITY_NUMBER = 255;
    private static final char[] CHARS_BY_DARKNESS = {'#', '@', 'X', 'L', 'I', ':', '.', ' '};

    private int min;
    private int max = RGB_COLORS_NUMBER * RGB_COLORS_INTENSITY_NUMBER;

    private BufferedImage image;

    private Image(String fileName) {

        this.image = loadImageFromFile(fileName);
    }

    /**
     * creates new image object for
     * the image specified by path.
     *
     * @param fileName path to image file
     * @return new image object for
     * the image specified by path
     */
    public static Image createImage(String fileName) {
        return new Image(fileName);
    }

    public int getMin() {
        return min;
    }


    /**
     * method sets min intensity value for image.
     *
     * @param min proposed value
     */
    public void setMin(int min) {
        if (min < this.min) {
            this.min = min;
        }
    }

    public int getMax() {
        return max;
    }

    /**
     * method sets max intensity value for image.
     *
     * @param max proposed value
     */
    public void setMax(int max) {
        if (max > this.max) {
            this.max = max;
        }
    }

    public int getHeight() {
        return image.getHeight();
    }

    public int getWidth() {
        return image.getWidth();
    }

    /**
     * returns intensity of pixel.
     *
     * @param coordinate object for pixel
     * @return sum of color codes
     */
    public int getIntensity(Coordinate coordinate) {
        return getRed(coordinate) + getBlue(coordinate) + getGreen(coordinate);
    }

    /**
     * returns red code of pixel.
     *
     * @param coordinate object for pixel
     * @return red code
     */
    public int getRed(Coordinate coordinate) {
        int rgbValue = getRgbValue(coordinate);
        return (rgbValue >> TWO_BYTES) & LAST_BYTE;
    }

    /**
     * returns green code of pixel.
     *
     * @param coordinate object for pixel
     * @return green code
     */
    public int getGreen(Coordinate coordinate) {
        int rgbValue = getRgbValue(coordinate);
        return (rgbValue >> BYTE) & LAST_BYTE;
    }

    /**
     * returns blue code of pixel.
     *
     * @param coordinate object for pixel
     * @return blue code
     */
    public int getBlue(Coordinate coordinate) {
        int rgbValue = getRgbValue(coordinate);
        return rgbValue & LAST_BYTE;
    }

    private int getRgbValue(Coordinate coordinate) {
        if (coordinate.getX() < 0 || coordinate.getX() > image.getWidth()) {
            throw new RuntimeException("Coordinate x out of range: 0.." + image.getWidth());
        } else if (coordinate.getY() < 0 || coordinate.getY() > image.getHeight()) {
            throw new RuntimeException("Coordinate y out of range: 0.." + image.getHeight());
        }
        return image.getRGB(coordinate.getX(), coordinate.getY());
    }

    private BufferedImage loadImageFromFile(String fileName) {
        try {
            return ImageIO.read(getClass().getClassLoader().getResource(fileName));
        } catch (IOException exception) {
            throw new RuntimeException("File not found!", exception);
        }
    }

    /**
     * gets char value for intensity.
     *
     * @param intensity average intensity for window
     * @return char symbol representing window with given intensity
     */
    public char getCharByIntensity(int intensity) {
        int index = (intensity - getMin()) * CHARS_BY_DARKNESS.length / (getMax() - getMin() + 1);
        return CHARS_BY_DARKNESS[index];
    }

}
