package com.epam.training.exercise2;

import java.util.Calendar;
import java.util.Date;

/**
 * date util class.
 */
public final class DateUtil {

    private static final int TEST_YEAR = 2014;
    private static final int TEST_MONTH = 10;
    private static final int TEST_DAY = 10;

    private DateUtil() {
    }

    /**
     * increment/decrement date to 1 day.
     *
     * @param date to increment
     * @param up   is true for incrementing,
     *             false for decrementing
     */
    public static void increment(final Date date, final boolean up) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, up ? 1 : -1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date.setTime(calendar.getTime().getTime());
    }

    /**
     * creates date for specified year, month, day.
     *
     * @param year  as int
     * @param month as int
     * @param day   as int
     * @return Date object
     */
    public static Date create(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    /**
     * checks date util functionality.
     *
     * @param args for jvm
     */
    public static void main(String[] args) {
        Date date = new Date();
        increment(date, false);
        System.out.println(date);

        System.out.println(DateUtil.create(TEST_YEAR, TEST_MONTH, TEST_DAY));
    }
}
