package com.epam.training.exercise3;

import java.util.ArrayList;
import java.util.List;

import com.epam.training.exercise3.boards.AvailabilityBoard;
import com.epam.training.exercise3.boards.Board;
import com.epam.training.exercise3.boards.Cell;
import com.epam.training.exercise3.boards.SolutionsBoard;

/**
 * class solves 8 queen problem.
 */
public final class EightQueens {
    private static final int CHESSBOARD_SIZE = 8;
    private static List<Board> solutions = new ArrayList<>();

    private EightQueens() {
    }

    /**
     * entry point.
     *
     * @param args for jvm
     */
    public static void main(String[] args) {
        resolveQueensProblem();
        System.out.println("\nSOLUTIONS (" + solutions.size() + ")\n");
        solutions.forEach(b -> System.out.println(b + "\n"));
    }

    private static void resolveQueensProblem() {
        for (int i = 0; i < CHESSBOARD_SIZE; i++) {
            AvailabilityBoard availabilityBoard = new AvailabilityBoard(CHESSBOARD_SIZE);
            SolutionsBoard solutionBoard = new SolutionsBoard(CHESSBOARD_SIZE);
            solutionBoard.setQueenCell(i, 0);
            availabilityBoard.setUnavailableCells(i, 0);
            solveBoard(solutionBoard, availabilityBoard, new Cell(true, i, 0));
        }
    }

    private static void solveBoard(SolutionsBoard solutionsBoard, AvailabilityBoard availabilityBoard, Cell queenCell) {
        if (availabilityBoard.countAvailable() == 0) {
            if (solutionsBoard.countQueens() == CHESSBOARD_SIZE) {
                addToSolutions(solutionsBoard);
            }
        } else {
            availabilityBoard.getCells().stream().filter(c -> c.getY() > queenCell.getY()).filter(Cell::isValue).forEach(c -> {
                SolutionsBoard currentSolution = solutionsBoard.getCopy();
                AvailabilityBoard currentAvailability = availabilityBoard.getCopy();
                currentSolution.setQueenCell(c.getX(), c.getY());
                currentAvailability.setUnavailableCells(c.getX(), c.getY());
                solveBoard(currentSolution, currentAvailability, c);
            });
        }
    }

    private static void addToSolutions(SolutionsBoard solutionsBoard) {
        if (!solutions.contains(solutionsBoard)) {
            solutions.add(solutionsBoard);
        }
    }

}
