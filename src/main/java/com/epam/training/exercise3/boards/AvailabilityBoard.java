package com.epam.training.exercise3.boards;

import java.util.ArrayList;
import java.util.List;

/**
 * class to represent available cells.
 */
public class AvailabilityBoard extends Board {

    public AvailabilityBoard(int size) {
        super(size, true);
    }

    public AvailabilityBoard(int size, boolean shouldFillCells) {
        super(size, true, shouldFillCells);
    }

    /**
     * sets unavailable cells if quin is placed on coordinates.
     *
     * @param x coordinate
     * @param y coordinate
     */
    public void setUnavailableCells(int x, int y) {
        getCells().stream().filter(c ->
                c.getX() == x
                        || c.getY() == y
                        || c.getX() - c.getY() == x - y
                        || c.getX() + c.getY() == x + y).forEach(c -> c.setValue(false));
    }

    /**
     * number of cells available.
     *
     * @return number of cells available.
     */
    public int countAvailable() {
        return (int) getCells().stream().filter(Cell::isValue).count();
    }

    /**
     * returns copy of current object.
     *
     * @return copy of current object.
     */
    public AvailabilityBoard getCopy() {
        AvailabilityBoard dest = new AvailabilityBoard(getSize(), false);
        List<Cell> newCells = new ArrayList<>();
        getCells().forEach(c -> newCells.add(c.getCopy()));
        dest.setCells(newCells);
        return dest;
    }
}
