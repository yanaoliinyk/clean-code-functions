package com.epam.training.exercise3.boards;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * class representing chess board.
 */
public abstract class Board {

    private List<Cell> cells;
    private int size;
    private boolean initialValue;

    public Board(int size, boolean initialValue) {
        this.size = size;
        this.initialValue = initialValue;
        cells = new ArrayList<>();
        fillCells();
    }

    public Board(int size, boolean initialValue, boolean shouldFillCells) {
        this.size = size;
        this.initialValue = initialValue;
        cells = new ArrayList<>();
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private void fillCells() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cells.add(new Cell(initialValue, j, i));
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        cells.forEach(c -> {
            if (c.isValue()) {
                sb.append(" X ");
            } else {
                sb.append(" . ");
            }
            if (c.getX() == size - 1) {
                sb.append("\n");
            }
        });
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Board board = (Board) o;
        return size == board.size
                && initialValue == board.initialValue
                && cells.equals(board.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cells, size, initialValue);
    }
}
