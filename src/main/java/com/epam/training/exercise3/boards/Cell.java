package com.epam.training.exercise3.boards;

import java.util.Objects;

/**
 * class represents cell of board.
 */
public class Cell {
    private boolean value;
    private int x;
    private int y;

    public Cell(boolean value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Cell getCopy() {
        return new Cell(value, x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cell cell = (Cell) o;
        return value == cell.value
                && x == cell.x
                && y == cell.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, x, y);
    }
}
