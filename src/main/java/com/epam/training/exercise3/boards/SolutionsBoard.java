package com.epam.training.exercise3.boards;

import java.util.ArrayList;
import java.util.List;

/**
 * class represents board with queens placed.
 */
public class SolutionsBoard extends Board {
    public SolutionsBoard(int size) {
        super(size, false);
    }

    public SolutionsBoard(int size, boolean shouldFillCells) {
        super(size, true, shouldFillCells);
    }

    /**
     * places queen on board.
     *
     * @param x coordinate
     * @param y coordinate
     */
    public void setQueenCell(int x, int y) {
        getCells().stream().filter(c -> c.getX() == x && c.getY() == y).forEach(c -> c.setValue(true));
    }

    /**
     * number of queens on board.
     *
     * @return number of queens on board.
     */
    public int countQueens() {
        return (int) getCells().stream().filter(Cell::isValue).count();
    }

    /**
     * copy of board.
     *
     * @return copy of board.
     */
    public SolutionsBoard getCopy() {
        SolutionsBoard dest = new SolutionsBoard(getSize(), false);
        List<Cell> newCells = new ArrayList<>();
        getCells().forEach(c -> newCells.add(c.getCopy()));
        dest.setCells(newCells);
        return dest;
    }
}
